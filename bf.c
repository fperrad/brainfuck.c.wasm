
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <emscripten/emscripten.h>

static char mem[30000];

void EMSCRIPTEN_KEEPALIVE eval_bf(const char* prg)
{
    int len = strlen(prg);
    const char *ip = prg;
    char *ptr = &mem[0];
    memset(ptr, 0, sizeof mem);

    while (ip < &prg[len]) {
        int count = 0;
        switch (*ip) {
        case '>':
            ++ptr;
            break;
        case '<':
            --ptr;
            break;
        case '+':
            ++(*ptr);
            break;
        case '-':
            --(*ptr);
            break;
        case '.':
            putc(*ptr, stdout);
            fflush(stdout);
            break;
        case ',':
            *ptr = getc(stdin);
            if (*ptr == EOF)
                exit(0);
            break;
        case '[':
            if (!*ptr) {
                count = 1;
                while (count) {
                    ++ip;
                    if (*ip == '[')
                        ++count;
                    if (*ip == ']')
                        --count;
                }
            }
            break;
        case ']':
            if (*ptr) {
                count = 1;
                while (count) {
                    --ip;
                    if (*ip == ']')
                        ++count;
                    if (*ip == '[')
                        --count;
                }
            }
            break;
        default:
            break;
        }
        ++ip;
    }
}
