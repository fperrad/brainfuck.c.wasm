
CFLAGS= -O2
OPTIONS=-s EXPORTED_RUNTIME_METHODS="['ccall', 'cwrap']"
EMCC=emcc

.PHONY: all
all: bf.js

.PHONY: pages
pages:
	mkdir public
	cp index.html   public/index.html
	cp bf.js        public/bf.js
	cp bf.wasm      public/bf.wasm

bf.js: bf.c
	$(EMCC) $(CFLAGS) $(OPTIONS) $< -o $@

.PHONY: clean
clean:
	rm -f bf.js bf.wasm

.PHONY: version
version:
	@emcc --version | head -n 1 | sed -e "s/^.*) //;s/ .*$$//;"
